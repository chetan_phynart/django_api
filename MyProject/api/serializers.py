from rest_framework import serializers
from .models import SingleFilm, User
from djoser.serializers import UserCreateSerializer


class SingleFilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = SingleFilm
        fields = ['id', 'title', 'director', 'type', 'country', 'premierDate', 'trailer']


class RegisterUserSerializer(UserCreateSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password', 'first_name', 'last_name', 'phone']

