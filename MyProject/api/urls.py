
from django.urls import path, include
from .views import add_film, get_films, single_film,get_single_film

urlpatterns = [
    path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    path('add_film/', add_film),
    path('get_films/', get_films),
    path('get_film/<int:id>/', get_single_film),
    path('film/<int:id>/', single_film),
]
