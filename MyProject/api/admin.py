from django.contrib import admin
from .models import SingleFilm, User
# Register your models here

admin.site.register(SingleFilm)
admin.site.register(User)
