from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from .models import SingleFilm
from .serializers import SingleFilmSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response


@csrf_exempt
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_film(request, *args, **kwargs):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = SingleFilmSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


def get_films(request):
    if request.method == 'GET':
        films = SingleFilm.objects.all()
    serializer = SingleFilmSerializer(films, many=True)
    return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def get_single_film(request, id):
    try:
        film = SingleFilm.objects.get(id=id)

    except SingleFilm.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = SingleFilmSerializer(film)
        return JsonResponse(serializer.data)


@csrf_exempt
@api_view(['PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
def single_film(request, id):
    try:
        film = SingleFilm.objects.get(id=id)

    except SingleFilm.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = SingleFilmSerializer(film, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        film.delete()
        return HttpResponse(status=204)
